package decaf;
import org.antlr.symtab.FunctionSymbol;
import org.antlr.symtab.GlobalScope;
import org.antlr.symtab.LocalScope;
import org.antlr.symtab.Scope;
import org.antlr.symtab.VariableSymbol;
import org.antlr.symtab.Symbol;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.ArrayList;


/**
 * This class defines basic symbols and scopes for Decaf language
 */
public class DecafSymbolsAndScopes extends DecafParserBaseListener {
    ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
    GlobalScope globals;
    Scope currentScope; // define symbols in this scope
    Boolean tipoVoid;
    String tipoMetodo;

	ArrayList<String> variaveis  = new ArrayList();
	ArrayList<String> escopos    = new ArrayList();
    ArrayList<String> metodos	 = new ArrayList(); 
    ArrayList<String> tiposParam = new ArrayList(); 
    ArrayList<String> variaveisAndTypes = new ArrayList();

	Boolean main = false;

    @Override
    public void enterProgram(DecafParser.ProgramContext ctx) {
        globals = new GlobalScope(null);

		for(int ct=0; ct < ctx.method_decl().size();ct++){
			if(ctx.method_decl().get(ct).ID().getText().equals("main")){
				this.main = true;
			}
		}		

        pushScope(globals);
    }

    @Override
    public void exitProgram(DecafParser.ProgramContext ctx) {
        System.out.println(globals);
        System.out.println(variaveis.toString());
		
		if(this.main == false){
            System.out.println("não possui main");
            System.exit(0);	
        } 	
		
    }

    @Override
    public void enterMethod_decl(DecafParser.Method_declContext ctx) {
        String name = ctx.ID().getText();
        escopos.add(name);
        tipoVoid =false;
        
        this.metodos.add("{"+name+","+ ctx.decl().size()+"}");

        for (int i = 0; i < ctx.decl().size(); i++) {
            this.tiposParam.add("{"+name+","+i+","+ctx.decl().get(i).type().getText()+"}");
        }

		//System.out.println("Escopos: "+name);

        //int typeTokenType = ctx.type().start.getType();
        //DecafSymbol.Type type = this.getType(typeTokenType);

        // push new scope by making new one that points to enclosing scope
        FunctionSymbol function = new FunctionSymbol(name);
        // function.setType(type); // Set symbol type
        
        String var = ctx.getText();
        if(var.startsWith("void")){
            tipoVoid =true;
        }
        try{
            this.tipoMetodo = ctx.type().getText();
        }catch(Exception e){

        }


        currentScope.define(function); // Define function in current scope
        saveScope(ctx, function);
        pushScope(function);
    }

    @Override
    public void exitMethod_decl(DecafParser.Method_declContext ctx) {
        try{
            if(tipoVoid == true){
                for(int i=0;i<ctx.bloco().statement().size();i++){
                    if(ctx.bloco().statement().get(i).RETURN().getText().equals("return")){
                        this.error(ctx.bloco().statement().get(i).RETURN().getSymbol(),"Metodo void não deve ter retorno");
                        System.exit(0);
                    }
                }       
            } else {
                for(int i=0;i<ctx.bloco().statement().size();i++){
                    if(ctx.bloco().statement().get(i).RETURN().getText().equals("return")){
                        for(int ct=0;ct<ctx.bloco().statement().get(i).decl_exp().size();ct++){
                            if(ctx.bloco().statement().get(i).decl_exp().get(ct).decl_literal() != null) {
                                String literal = ctx.bloco().statement().get(i).decl_exp().get(ct).decl_literal().getText();

                                if(this.tipoMetodo.equals("boolean") && !(literal.equals("false") || literal.equals("true"))){
                                    this.error(ctx.ID().getSymbol(),"o valor do retorno está errado");
                                    System.exit(0);
                                }
                                if(this.tipoMetodo.equals("int") && !(this.soNumeros(literal))){
                                    this.error(ctx.ID().getSymbol(),"o valor do retorno está errado");
                                    System.exit(0);
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception e){

        }
    }
    
	@Override 
	public void enterStatement(DecafParser.StatementContext ctx) { 
		try{
            
            if(ctx.IF() != null){
                for(int i=0;i<ctx.decl_exp().size();i++) {
                    if(!ctx.decl_exp().get(i).location().ID().getText().equals("true")
                        || !ctx.decl_exp().get(i).location().ID().getText().equals("false")
                        || !this.variaveisAndTypes.contains("{"+ctx.decl_exp().get(i).location().ID().getText()+","+"boolean"+"}")
                        || !this.variaveisAndTypes.contains("{"+ctx.decl_exp().get(i).location().ID().getText()+","+"boolean"+","+"array"+"}")){

                        this.error(ctx.decl_exp().get(i).location().ID().getSymbol(),"condition should be a boolean");
                        System.exit(0);
                    }
                }
            }

            if(ctx.FOR() != null){
                if(ctx.decl_exp().get(0).getText().equals("true")
                        || ctx.decl_exp().get(0).getText().equals("false")
                        || !this.variaveisAndTypes.contains("{"+ctx.decl_exp().get(0).location().ID().getText()+","+"int"+"}")){
                    this.error(ctx.ID().getSymbol(),"condicao inicial deve ser int");
                    System.exit(0);
                }
            }

			String var = ctx.location().ID().getText();
			if(!this.variaveis.contains(var)){
				this.error(ctx.location().ID().getSymbol(), "A variavel "+var+" não foi declarada");			
				System.exit(0);	
					
			}
			String retornoInt = ctx.getText();
			if(retornoInt.contains("+=") || retornoInt.contains("-=")){
				if(retornoInt.contains("true") || retornoInt.contains("false")){
					this.error(ctx.location().ID().getSymbol(), " para incrementar ou decrementar um valor ele deve ser inteiro");
					System.exit(0);
				}			
            }


            if(ctx.location().LCOLCHETE() != null){
                try{
                    int verify = Integer.parseInt(ctx.location().decl_exp().getText());
                }catch (NumberFormatException e){
                    if(!this.variaveisAndTypes.contains("{"+ctx.location().decl_exp().getText()+","+"int"+"}")){
                        this.error(ctx.location().ID().getSymbol(),"o tipo do indice está errado");
                        System.exit(0);
                    }
                }
            }
            if(ctx.operadores().getText() != null){
                String verifyType = "";
                String verifyArray = "";

                if(this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"int"+","+"array"+"}")){
                    verifyType = "int";
                    verifyArray = "array";
                }else if(this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"boolean"+","+"array"+"}")){
                    verifyType = "boolean";
                    verifyArray = "array";
                }else if (this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"int"+"}")){
                    verifyType = "int";
                    verifyArray = null;
                }else if (this.variaveisAndTypes.contains("{"+ctx.location().ID().getText()+","+"boolean"+"}")){
                    verifyType = "boolean";
                    verifyArray = null;
                }

                for(int i=0;i<ctx.decl_exp().size();i++){
                    if(verifyType == "int" && this.variaveisAndTypes.contains("{"+ctx.decl_exp().get(i).getText()+","+"int"+","+"array"+"}")){
                        this.error(ctx.location().ID().getSymbol(), "bad type, rhs should be an int");
                        System.exit(0);
                    }

                    if (verifyType=="int" && ctx.decl_exp().get(i).bin_op().OP_COMPARACAO().getText() != null){
                        this.error(ctx.location().ID().getSymbol(),"o valor deveria ser uma expressao inteira");
                        System.exit(0);
                    }


                    if(ctx.decl_exp().get(i).getText().contains("!")){
                        if(!ctx.decl_exp().get(i).decl_exp().get(0).getText().equals("true")
                            && !ctx.decl_exp().get(i).decl_exp().get(0).getText().equals("false")

                        ){
                            this.error(ctx.location().ID().getSymbol(),"operador de ! deve ser boolean");
                            System.exit(0);
                        }
                    }
                }

                if(ctx.decl_exp().get(0).bin_op().OP_COMPARACAO() != null){
                    for(int i=0;i<ctx.decl_exp().get(0).decl_exp().size();i++){
                        if(ctx.decl_exp().get(0).decl_exp(i).getText().equals("true") || ctx.decl_exp().get(0).decl_exp(i).getText().equals("false")){
                            this.error(ctx.location().ID().getSymbol(),"operadores > devem ser ints");
                            System.exit(0);
                        }
                    }
                }
                if(ctx.decl_exp().get(0).bin_op().OP_IGUALDADE() != null){ 
                    for(int i=0;i<ctx.decl_exp().get(0).decl_exp().size();i++){ 
                        if(ctx.decl_exp().get(0).decl_exp(i).getText().equals("true") || ctx.decl_exp().get(0).decl_exp(i).getText().equals("false")){
                            this.error(ctx.location().ID().getSymbol(),"os tipos de operadores devem ser iguais");
                            System.exit(0);
                        }
                    }
                }

            }
						
		} catch (Exception e) {
			//System.out.println("Erro: "+e);
		}
		
	}

    @Override 
    public void enterMetodo(DecafParser.MetodoContext ctx) {
        try{
            if(!this.metodos.contains("{"+ctx.method_name().ID().getText()+","+ctx.decl_exp().size()+"}")){
                this.error(ctx.method_name().ID().getSymbol(),"numero de parametros invalido");
                System.exit(0);
            }

            for(int ct=0;ct<ctx.decl_exp().size();ct++){
                if(ctx.decl_exp().get(ct).decl_literal().getText().equals("false")||ctx.decl_exp().get(ct).decl_literal().getText().equals("true")){
                    if(!this.tiposParam.contains("{"+ctx.method_name().ID().getText()+","+ct+",boolean}")){
                        this.error(ctx.method_name().ID().getSymbol(),"o tipo do parametro está errado");
                        System.exit(0);
                    }
                } else{
                    if(!this.tiposParam.contains("{"+ctx.method_name().ID().getText()+","+ct+",int}")){
                        this.error(ctx.method_name().ID().getSymbol(),"o tipo do parametro está errado");
                        System.exit(0);
                    }
                }
            }

        } catch(Exception e) {
            
        }
    }

	@Override public void exitStatement(DecafParser.StatementContext ctx) { 
		
	} 
	/*
    @Override
    public void enterBloco(DecafParser.BlocoContext ctx) {
        LocalScope l = new LocalScope(currentScope);
        saveScope(ctx, currentScope);
        //pushScope(l);
    }

    @Override
    public void exitBloco(DecafParser.BlocoContext ctx) {
        popScope();
    } */

    @Override 
    public void enterLocation(DecafParser.LocationContext ctx) { 
    
    }
	

    @Override
    public void enterDecl(DecafParser.DeclContext ctx) {
        defineVar(ctx.type(), ctx.ID().getSymbol());
        this.variaveis.add(ctx.ID().getText());
        this.variaveisAndTypes.add("{"+ctx.ID().getText()+","+ctx.type().getText()+"}");
    }

    @Override
    public void exitDecl(DecafParser.DeclContext ctx) {
        String name = ctx.ID().getSymbol().getText();
        Symbol var = currentScope.resolve(name);
        if ( var==null ) {
            this.error(ctx.ID().getSymbol(), "no such variable: "+name);
        }
        if ( var instanceof FunctionSymbol ) {
            this.error(ctx.ID().getSymbol(), name+" is not a variable");
        }
    }
    @Override 
	public void enterDecl_var(DecafParser.Decl_varContext ctx) { 
		for(int ct=0; ct < ctx.ID().size();ct++){
            variaveis.add(ctx.ID().get(ct).getText());
            this.variaveisAndTypes.add("{"+ctx.ID().get(ct).getText()+","+ctx.decl().type().getText()+"}");
			defineVar(ctx.ID().get(ct).getSymbol());	
		}
	}

    @Override 
    public void exitDecl_var(DecafParser.Decl_varContext ctx) { 
    	for(int ct=0; ct<ctx.ID().size(); ct++){
			String texto = ctx.ID().get(ct).getSymbol().getText();
			Symbol var   = currentScope.resolve(texto);
		
			if(var==null){
				this.error(ctx.ID().get(ct).getSymbol(), " variavel "+ texto +" não existe");
				System.exit(0);		
			}
			if(var instanceof FunctionSymbol){
				this.error(ctx.ID().get(ct).getSymbol(), texto+" não é uma variável");
				System.exit(0);
			}
		}
    
	}

	@Override 
	public void enterField_decl(DecafParser.Field_declContext ctx) {
        try{
            String valor = ctx.INTLITERAL().getText(); 
            if(Integer.parseInt(valor) <= 0){  
                this.error(ctx.INTLITERAL().getSymbol(), "Tamanho de array inválido: "+valor);
                System.exit(0);
            }
        }catch (Exception e) {  }
        
        
        for(int i=0; i<ctx.ID().size(); i++){
            String field = ctx.ID().get(i).getSymbol().getText();
            this.variaveis.add(field);
            System.out.println("teste:" + ctx);
            if(ctx.LCOLCHETE() == null){
                this.variaveisAndTypes.add("{"+field+","+ctx.decl().type().getText()+"}");
            }else{
                this.variaveisAndTypes.add("{"+field+","+ctx.decl().type().getText()+","+"array"+"}");
            }

            VariableSymbol fieldSymbol = new VariableSymbol(field);
            currentScope.define(fieldSymbol);		
        }
        
		
		
	}
	
	@Override public void exitField_decl(DecafParser.Field_declContext ctx) { 
		for(int ct=0; ct<ctx.ID().size(); ct++){
			
			String name  = ctx.ID().get(ct).getSymbol().getText();
			Symbol field = currentScope.resolve(name);

			if(field == null){
				this.error(ctx.ID().get(ct).getSymbol(), "no such variable: "+ name);
			}
			if(field instanceof FunctionSymbol) {
				this.error(ctx.ID().get(ct).getSymbol(), name + " is not a variable");			
			}
		}
	}
    void defineVar(DecafParser.TypeContext typeCtx, Token nameToken) {
        int typeTokenType = typeCtx.start.getType();
        VariableSymbol var = new VariableSymbol(nameToken.getText());

        //DecafSymbol.Type type = this.getType(typeTokenType);
        //var.setType(type);

        currentScope.define(var); // Define symbol in current scope
    }

    void defineVar(Token nameToken){
        VariableSymbol var = new VariableSymbol(nameToken.getText());
        currentScope.define(var);

    }

    /**
     * Método que atuliza o escopo para o atual e imprime o valor
     *
     * @param s
     */
    private void pushScope(Scope s) {
        currentScope = s;
        System.out.println("entering: "+currentScope.getName()+":"+s);
    }

    /**
     * Método que cria um novo escopo no contexto fornecido
     *
     * @param ctx
     * @param s
     */
    void saveScope(ParserRuleContext ctx, Scope s) {
        scopes.put(ctx, s);
    }

    /**
     * Muda para o contexto superior e atualia o escopo
     */
    private void popScope() {
        System.out.println("leaving: "+currentScope.getName()+":"+currentScope);
        currentScope = currentScope.getEnclosingScope();
    }

    public static void error(Token t, String msg) {
        System.err.printf("line %d:%d %s\n", t.getLine(), t.getCharPositionInLine(),
                msg);
    }

    /**
     * Valida tipos encontrados na linguagem para tipos reais
     *
     * @param tokenType
     * @return
     */
    public static DecafSymbol.Type getType(int tokenType) {
        switch ( tokenType ) {
            case DecafParser.VOID :  return DecafSymbol.Type.tVOID;
            case DecafParser.INT :   return DecafSymbol.Type.tINT;
            
        }
        return DecafSymbol.Type.tINVALID;
    }

    public boolean soNumeros(String texto){
        for(int ct=0;ct<texto.length();ct++){
            if(!Character.isDigit(texto.charAt(ct))){
                return false;
            }
        }
        return true;
    }

}
