parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: CLASS PROGRAMA LCURLY (field_decl)* (method_decl)* RCURLY EOF;

bloco:  LCURLY (decl_var)* (statement)* RCURLY;

method_decl: (type | VOID) ID LPARENTESE ((decl) (VIRGULA decl)*)? RPARENTESE bloco;

decl_var: decl (VIRGULA ID)* PONTOVIRGULA;


type: (INT | BOOLEAN);

field_decl: (decl | decl LCOLCHETE INTLITERAL RCOLCHETE) (VIRGULA ID)* PONTOVIRGULA;

statement: location operadores decl_exp PONTOVIRGULA
	| metodo PONTOVIRGULA
	| IF LPARENTESE decl_exp RPARENTESE bloco  (ELSE bloco)?
	| FOR ID OP_ATRIBUICAO2 decl_exp VIRGULA decl_exp bloco
	| RETURN decl_exp? PONTOVIRGULA
	| BREAK PONTOVIRGULA
	| CONTINUE PONTOVIRGULA
	| bloco;	

operadores: (OP_ATRIBUICAO | OP_ATRIBUICAO2);

decl_exp: decl_literal 
| metodo 
| location
| decl_exp bin_op decl_exp
| MENOS decl_exp
| EXCLAMACAO decl_exp
| LPARENTESE decl_exp RPARENTESE;

bin_op: op_arit
| OP_COMPARACAO
| OP_IGUALDADE
| OP_E_OU;

location: ID | ID LCOLCHETE decl_exp RCOLCHETE;


metodo: method_name LPARENTESE ((decl_exp) ((VIRGULA decl_exp)+)?)? RPARENTESE
| CALLOUT LPARENTESE STRING (VIRGULA callout_arg (VIRGULA callout_arg)*)? RPARENTESE;

method_name: ID;

callout_arg: decl_exp | STRING;
decl_literal: BOOLEANLITERAL 
| INTLITERAL 
| CHAR;

op_arit: MAIS | MENOS | VEZES | DIVISAO | PERCENT;
decl: type ID;
