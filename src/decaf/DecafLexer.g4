lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

tokens
{
  TK_class
}
PROGRAMA: 'Program';
LCURLY : '{';
RCURLY : '}';
LPARENTESE : '(';
RPARENTESE : ')';
LCOLCHETE : '[';
RCOLCHETE : ']';

IF: 'if';
ELSE: 'else';
FOR: 'for';
INT: 'int';
BOOLEAN: 'boolean';
CALLOUT: 'callout';
VOID: 'void';
CLASS: 'class';
RETURN: 'return';
BREAK: 'break';
CONTINUE: 'continue';

OP_COMPARACAO: ('<'|'>'|'>='|'<=');
OP_E_OU: ('&&'|'||');
OP_IGUALDADE: ('=='|'!=');
OP_ATRIBUICAO: ('+='|'-=');
OP_ATRIBUICAO2: '=';

MENOS: '-';
MAIS: '+';
VEZES: '*';
DIVISAO: '/';
PERCENT: '%';
EXCLAMACAO: '!';

BOOLEANLITERAL: ('true'|'false');
ID: ('a'..'z' | 'A'..'Z' | '_' )+ ([0-9])* ID?;

WS_ : (' ' | '\n' | '\t' |'\\\\') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' (ESC|[ !#-&(-.0-Z^-~)]) '\'';
STRING : '"' (ID|SINPONTUACAO|ESC|ESPECIALASK|PERCENT)+ '"';

INTLITERAL : [0-9]+ ('x'([a-zA-Z]|[0-9])+)?;


PONTOVIRGULA: ';';
VIRGULA: ',';

fragment ESC : '\\' ('n'|'t'|'\\'|'\"'|'"');
fragment SINPONTUACAO : ('?'|','|';'| ESPECIAL|'.'|' '|':');
fragment ESPECIAL : '\\' ('\''|'\"'|'\\');
fragment ESPECIALASK : '\\' ([ !#-&(-/:-@\-`]);





